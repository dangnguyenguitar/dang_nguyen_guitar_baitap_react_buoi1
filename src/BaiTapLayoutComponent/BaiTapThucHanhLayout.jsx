import React, { Component } from "react";
import Header from "./Header";
import Banner from "./Banner";
import Item from "./Item";
import Footer from "./Footer";
export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <>
        <div className="header text-white bg-dark">
          <Header />
        </div>
        <div className="body">
          <div className="row justify-content-center">
            <div className="col-8">
              <Banner />
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              <Item />
            </div>
          </div>
        </div>
        <div className="footer text-white bg-dark">
          <Footer />
        </div>
      </>
    );
  }
}
