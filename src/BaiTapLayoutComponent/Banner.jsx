import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div className="bg-light text-left my-3 p-5">
        <h2>A Warm Welcome!</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium
          aliquam soluta numquam adipisci? Reprehenderit eos at ipsa fugit eum
          voluptates quod quos natus, optio alias, expedita cum dolores vitae
          dolore!
        </p>
        <button
          type="button"
          name=""
          id=""
          class="btn btn-primary"
          btn-lg
          btn-block
        >
          Call to action!
        </button>
      </div>
    );
  }
}
