import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div>
        <div className="card-item pb-3">
          <div className="row">
            <div className="col-3 p-2">
              <div className="img-container">
                <img
                  src="https://i.picsum.photos/id/964/500/325.jpg?hmac=C6NDAvBGhVVtElRk_xISI-qbPJHAwlt6emwlZrEyRY0"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <div className="card-content p-2">
                <h4>Card title</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint
                  natus et autem quis molestias nobis quibusdam, dolore atque
                  reiciendis perspiciatis!
                </p>
              </div>
              <div className="call-to-action bg-light py-3">
                <button
                  type="button"
                  name=""
                  id=""
                  class="btn btn-primary"
                  btn-lg
                  btn-block
                >
                  Find Out More!
                </button>
              </div>
            </div>
            <div className="col-3 p-2">
              <div className="img-container">
                <img
                  src="https://i.picsum.photos/id/964/500/325.jpg?hmac=C6NDAvBGhVVtElRk_xISI-qbPJHAwlt6emwlZrEyRY0"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <div className="card-content p-2">
                <h4>Card title</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint
                  natus et autem quis molestias nobis quibusdam, dolore atque
                  reiciendis perspiciatis!
                </p>
              </div>
              <div className="call-to-action bg-light py-3">
                <button
                  type="button"
                  name=""
                  id=""
                  class="btn btn-primary"
                  btn-lg
                  btn-block
                >
                  Find Out More!
                </button>
              </div>
            </div>
            <div className="col-3 p-2">
              <div className="img-container">
                <img
                  src="https://i.picsum.photos/id/964/500/325.jpg?hmac=C6NDAvBGhVVtElRk_xISI-qbPJHAwlt6emwlZrEyRY0"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <div className="card-content p-2">
                <h4>Card title</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint
                  natus et autem quis molestias nobis quibusdam, dolore atque
                  reiciendis perspiciatis!
                </p>
              </div>
              <div className="call-to-action bg-light py-3">
                <button
                  type="button"
                  name=""
                  id=""
                  class="btn btn-primary"
                  btn-lg
                  btn-block
                >
                  Find Out More!
                </button>
              </div>
            </div>
            <div className="col-3 p-2">
              <div className="img-container">
                <img
                  src="https://i.picsum.photos/id/964/500/325.jpg?hmac=C6NDAvBGhVVtElRk_xISI-qbPJHAwlt6emwlZrEyRY0"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <div className="card-content p-2">
                <h4>Card title</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint
                  natus et autem quis molestias nobis quibusdam, dolore atque
                  reiciendis perspiciatis!
                </p>
              </div>
              <div className="call-to-action bg-light py-3">
                <button
                  type="button"
                  name=""
                  id=""
                  class="btn btn-primary"
                  btn-lg
                  btn-block
                >
                  Find Out More!
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
